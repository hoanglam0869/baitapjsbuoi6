function showResult1() {
  var evenNum = "Số chẵn: ";
  var oddNum = "Số lẻ: ";
  for (var i = 1; i < 100; i++) {
    if (i % 2 == 0) {
      evenNum += `${i} `;
    } else {
      oddNum += `${i} `;
    }
  }
  document.getElementById("result-1-even").innerText = `👉 ${evenNum}`;
  document.getElementById("result-1-odd").innerText = `👉 ${oddNum}`;
}

function showResult2() {
  var count = 0;
  for (var i = 0; i < 1000; i++) {
    if (i % 3 == 0) {
      count++;
    }
  }
  document.getElementById(
    "result-2"
  ).innerText = `Số chia hết cho 3 nhỏ hơn 1000: ${count} số`;
}

function showResult3() {
  var sum = 0;
  var count = 0;
  while (sum <= 10000) {
    count++;
    sum += count;
  }
  document.getElementById(
    "result-3"
  ).innerText = `Số nguyên dương nhỏ nhất: ${count}`;
}

function showResult4() {
  var numX = document.getElementById("num-x").value * 1;
  var numN = document.getElementById("num-n").value * 1;

  var sum = 0;
  for (var i = 1; i <= numN; i++) {
    sum += Math.pow(numX, i);
  }
  document.getElementById("result-4").innerText = `Tổng: ${sum}`;
}

function showResult5() {
  var numN = document.getElementById("num-n-1").value * 1;

  /* var factorial = 1;
  for (var i = 1; i <= numN; i++) {
    factorial *= i;
  } */
  var factorial = _factorial(numN);
  document.getElementById("result-5").innerText = `Giai thừa: ${factorial}`;
}

function _factorial(num) {
  if (num <= 1) return 1;
  return num * _factorial(num - 1);
}

function showResult6() {
  var contentHTML = "";
  for (var i = 1; i < 10; i++) {
    if (i % 2 == 0) {
      contentHTML += `<div class="bg-danger text-white p-2">Div chẵn</div>`;
    } else {
      contentHTML += `<div class="bg-primary text-white p-2">Div lẻ</div>`;
    }
  }
  document.getElementById("result-6").innerHTML = contentHTML;
}

function showResult7() {
  var numN = document.getElementById("num-n-2").value * 1;

  var primeNumber = "";
  for (var i = 1; i <= numN; i++) {
    if (isPrimeNumber(i)) {
      primeNumber += `${i} `;
    }
  }
  document.getElementById("result-7").innerText = primeNumber;
}

function isPrimeNumber(num) {
  if (num == 1) return true;
  for (var i = 2; i < num; i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}
